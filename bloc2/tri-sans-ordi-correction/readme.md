Retour sur le tri sans ordinateur
================================

Il nous a paru nécessaire de formaliser davantage l'écriture des algorithmes en français avec pour objectif que les méthodes décrites doivent :
- ne pas être ambigues,
- préciser les objets sur lesquelles elles opèrent,
- ne pas être une implémentation en python
- être reproductibles par quelqu'un d'autre n'ayant aucune connaissance de la méthode.

Tous les algorithmes de tri décrits ici prennent en :
- Entrée : $N$ boîtes 
- Sortie : Les mêmes boîtes alignées de la plus légère à la plus grande.

## Le tri par sélection

### en français


#### Version 1 :
```
Je débute avec un alignement vide de boîtes triées
Tant qu'il y a des boîtes non triées :
   Je cherche la boite la plus légère parmi les boîtes non triées
   Je la place à la suite des boîtes déjà triées.
fin Tant que
```

Il est nécessaire de définir une méthode pour déterminer la plus légère des boîtes
parmi les boîtes non triées

```
Entrée : Des boîtes
Sortie : La boite la plus légère 
Effet de Bord : Enlève une boite 

Je prends une boîte 
Pour chacune des autres :
    Si cette boite est plus légère que la boite dans ma main,
    Alors je conserve la plus légère des deux.
	Fin Si
	Je mets l'autre de côté.
Fin Pour
```

#### Version 2:

On considère que les boîtes sont alignées et on les repère par leur position de 
la gauche vers la droite.

```
Pour i allant de la première position à la dernière
   trouver la boite la plus légère parmi les boîtes aux positions i à N
   échanger la boite à la position i avec cette boite
fin pour
```

Il est nécessaire de définir une méthode pour déterminer la plus petite des boîtes
parmi les positions i à N 

```
Entrées : les boîtes, une position i (de départ)
Sortie : la position de la boîte la plus légère

Je repère la première boite par sa position
pour la deuxième boîte jusqu'à la dernière
  Si la boîte courante est plus légère que la boîte repérée,
  Alors la boîte courante est repérée
fin pour
renvoie la position de la boîte repérée
```

### en python

```python

def tri_selection(l, cmp):
   """
   :param l: (list) une liste d'éléments comparables
   :param cmp: (fonction) une fonction de comparaison
   :return: None
   :Effet de Bord: trie les éléments de l selon l'ordre passé en paramètre
   """
   for i in range(len(l)):
      i_min = indice_min(l, i)
	  l[i] , l[i_min] = l[i_min], l[i]
	  
def indice_min(i, l, cmp):
	"""
	:param i: (int) un indice
	:param l: (list) une liste d'éléments comparables
	:param cmp: (fonction) une fonction de comparaison
	:CU: 0<= i < len(l)
	:return: l'indice du minimum de la liste l[i:]
	"""
	imin = i
	for j in range(i, len(l)):
	   if cmp(l[j], l[imin]) < 0:
	      imin = j
	return imin
```

### idée de l'algorithme

- on a la place, on selectionne l'élément 
- Invariant de la boucle principale : les $i$ premières boîtes sont triées et les boîtes de la partie non triées sont toutes de poids supérieur à celles triées.
- Invariant de la boucle dans la fonction de recherche du minimum : la boîte repérée est la plus légère parmi celles rangées aux positions comprises entre $i$ et $j$ (on considère j=i avant la boucle)

## Le tri par insertion

### en français

#### Version 1
```
    Je débute avec un alignement vide de boîtes triées
    Tant qu'il y a des boîtes non triées :
       Je choisis une boite 
	   Je l'insère dans l'alignement de telle sorte qu'il reste trié
    fin Tant 
```

Il est important ici de présenter un algorithme d'insertion dans un alignement trié.

```
Entree : Un alignement de boîtes trié, une boîte b
Sortie : rien
Effet de bord: l'alignement reste trié

Prends la boîte la plus à droite (la plus lourde)
Tant que cette boite est plus lourde que b
     passe à la boite suivante
insère b à la droite de la boite courante
```

#### Version 2
```
Pour i allant de la deuxième position à la dernière ($N$):
   insère la boite i dans les boîtes aux positions 1 à i-1
Fin Pour
```

```
Entree : une boite b à la position i

j = i-1
tant que la boite à la position j est plus légère que la boite b
   déplace la boite j vers la droite
fin tant que
insere la boite b dans l'espace libre
```

### Code python

```python
def inserer(l, i, comp=compare):
    """
    :param l: (list) une liste
    :param i: (int) indice de l'élément de l à insérer dans l[0:i+1]
    :param comp: (fonction) [optionnel] une fonction de comparaison
                             Valeur par défaut : compare
    :return: (NoneType) aucune
    :Effet de bord: insère l'élément l[i] à sa place dans la tranche l[0:i+1]
             de sorte que cette tranche soit triée si l[0:i] l'est auparavant
    :CU: 0 <= i < long(l)
         éléments de l comparables par comp
         la tranche l[0:i] est triée
    :Exemples:

    >>> l = [1, 2, 4, 5, 3, 7, 6]
    >>> inserer(l, 4)
    >>> l == [1, 2, 3, 4, 5, 7, 6]
    True
    >>> inserer(l, 5)
    >>> l == [1, 2, 3, 4, 5, 7, 6]
    True
    >>> inserer(l, 6)
    >>> l == [1, 2, 3, 4, 5, 6, 7]
    True
    """
    aux = l[i]
    k = i
    while k >= 1 and comp(aux, l[k - 1]) < 0:
        l[k] = l[k - 1]
        k = k - 1    
    l[k] = aux

def tri_insert(l, comp=compare):
    """
    :param l: (list) une liste à trier
    :param comp: (fonction) [optionnel] une fonction de comparaison
    :return: (NoneType) aucune
    :Effet de bord : modifie la liste l en triant ses éléments selon l'ordre défini par comp
          Algorithme du tri par insertion
    :CU: l liste homogène d'éléments comparables selon comp
    :Exemples:

    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_insert(l)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    >>> from random import randrange
    >>> l1 = [randrange(1000) for k in range(randrange(100))]
    >>> l2 = l1.copy()
    >>> tri_insert(l2)
    >>> est_trie(l2)
    True
    >>> all(l1.count(elt) == l2.count(elt) for elt in l1)
    True
    >>> all(l1.count(elt) == l2.count(elt) for elt in l2)
    True
    """
    n = len(l)
    # la tranche l[0:1] est triée
    for i in range(1, n):
        # supposons la tranche l[0:i] triée
        inserer (l, i, comp=comp)
        # alors la tranche l[0:i+1] est triée
    # à l'issue de l'itération la tranche l[0:n] est triée
```

### Idée de l'algorithme

- "J'ai l'élément, je le range à sa place"
- Invariant de la boucle principale : les $i$ premières boîtes sont rangées.
- Invariant de la boucle dans la fonction d'insertion : les boîtes à droite de b sont toutes plus légères que b.

## Le tri rapide 

### en français

#### Version 1

```
Entrée : un ensemble l de boîtes
Effet : les boîtes sont triées

S'il y a au moins deux boîtes dans l,
Alors 
  Choisis une boite b dans l
  Pour toutes les autres boîtes dans l
    	si la boîte est plus légère que b, range-la à gauche de b.
	    si la boîte est plus lourde que b, range-la à droite de b.
	    demande à un ami d'appliquer la même méthode pour les boîtes de droite, 
	    demande à un ami d'appliquer la même méthode pour les boîtes de gauche.
  fin Pour
Fin Si

```

### en python

```python
def partitionner(l, debut, fin, comp=compare):
    '''
    :param l: (list) liste à partitionner
    :param debut, fin: (int) indices de début et de fin de la tranche de l
                       à partionner
    :return: (int) indice où se trouve le pivot (initialement à l'indice debut)
                   après le partionnement
    :effet de bord: après partionnement tous les éléments de la tranche l[debut:fin]
                    ont été déplacés de sorte que ceux situés à gauche du pivot soient
                    situés à sa gauche, et les autres à sa droite
    :CU: 0 <= debut < fin <= len(l)
    :Exemples:

    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> ind_pivot = partitionner(l, 2, 6)
    >>> ind_pivot
    3
    >>> l
    [3, 1, 1, 4, 5, 9, 2]
    '''
    pivot = l[debut]
    ind_pivot = debut
    for i in range (debut+1, fin):
        if comp(pivot, l[i]) > 0:
            l[ind_pivot] = l[i]
            l[i] = l[ind_pivot+1]
            ind_pivot += 1
    l[ind_pivot] = pivot
    return ind_pivot
	
def tri_rapide(l, debut=0, fin=None, comp=compare):
    """
    :param l: (list) une liste à trier
    :param comp: (fonction) [optionnel] une fonction de comparaison
    :param debut, fin: (int) [optionnel] indices délimitant la tranche à trier
    :return: (NoneType) aucune
    :Effet de bord : modifie la liste l en triant 
          les éléments de la tranche l[debut:fin] selon l'ordre défini par comp
          Algorithme du tri rapide
    :CU: l liste homogène d'éléments comparables selon comp
    :Exemples:

    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_rapide(l)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    >>> from random import randrange
    >>> l1 = [randrange(1000) for k in range(randrange(100))]
    >>> l2 = l1.copy()
    >>> tri_rapide(l2)
    >>> est_trie(l2)
    True
    >>> all(l1.count(elt) == l2.count(elt) for elt in l1)
    True
    >>> all(l1.count(elt) == l2.count(elt) for elt in l2)
    True
    """
    if fin is None: fin = len(l)
    if fin - debut > 1:
        ind_pivot = partitionner(l, debut, fin, comp=comp)
        tri_rapide(l, debut=debut, fin=ind_pivot, comp=comp)
        tri_rapide(l, debut=ind_pivot+1, fin=fin, comp=comp)

```

### Idée de l'algorithme

- Les alignements constitués d'une seule boîte sont triés.
- Si toutes les boîtes situées à gauche sont triées et toutes les boîtes situées à droites sont
  triées, alors l'algorithme produit un alignement trié.




