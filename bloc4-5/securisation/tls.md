% TLS
% DIU-EIL

# Couche de sécurité

TLS signifie Transport Layer Sécurity ("sécurité de la couche de
transport"). Comme son nom l'indique, il s'agit de l'ensemble des
protocoles permettant la sécurisation de la couche de transport.

C'est le successeur de SSL (Secure Socket Layer).


# Missions

La TLS fonctionne en mode client-serveur. Voici les objectifs de ces
protocoles :

-   permettre l'**authentification** du serveur ;
-   garantir la **confidentialité** des données échangées ;
-   garantir l'**intégrité** des données échangées ;
-   permettre l'authentification du client (optionnellement).


# Principe de fonctionnement


## Client Hello

1.  Le client envoie au serveur un message **"client hello"**
    
    -   version supportée
    -   algorithmes de chiffrements symétriques supportés
    
    <div class="notes">
    -   Le message contient la version de TLS supportée par le
        navigateur.
    -   un premier nombre aléatoire est envoyé ici
    
    </div>

![img](./figs/TLS_1.png)


## Serveur Hello

2.  Le serveur répond et envoie au client un **"server hello"**,
    contenant :
    
    -   son **certificat**
    -   l'algorithme de chiffrement **symétrique** qu'il a choisi
    
    <div class="notes">
    -   algo de chiffrement,
    -   nombre aléatoire
    
    </div>

![](./figs/TLS_2.png)


## Authentification

1.  **Authentification** Le client 
    
    -   vérifie la signature numérique du certificat;
    
    <div class="notes">
    plusieurs cas de figure à envisager :
    
    -   la CA valide le certificat ;
    -   le certificat est autosigné ;
    -   le certificat n'est pas valide
    
    </div>
2.  Le client génère une clé de chiffrement symétrique, appelée 
    **clé de session**, chiffrée avec la clé publique du serveur
    
    <div class="notes">
    en fait client et serveur calculent la clé de session chacun de
    leur côté, en utilisant :
    
    -   tous les nombres aléatoires générés précédemment
    -   l'algorithme choisi
    -   les options de l'algorithme négociés
    
    </div>

![](./figs/TLS_3.png)


## Fin

1.  Le client envoie un message "Finished", chiffré avec la clé de
    session;
2.  Le serveur répond envoie un message "Finished", chiffré avec la
    clé de session.

![](./figs/TLS_4.png) 


# Wireshark

-   Analyseur de paquets réseau;
-   libre et gratuit;
-   Comprend les structures de différents protocoles de communication;
-   Options de tri et de filtrage des paquets;

![](./figs/Wireshark_icon.svg)

<div class="notes">
L'installation sur linux est aisée :

```shell
apt install wireshark

```

Accorder les privilèges à un utilisateur lambda :

```shell
usermod -a -G wireshark <login>

```

</div>

